from MetodosNum import MetodosNum
from MetodosNum import PenduloEuler

'''Execution asociado al problema del péndulo simple bajo la aproximación de pequeñas oscilaciones'''

if __name__ == "__main__":
    # definimos las condiciones y parametros iniciales
    theta0 = 1  # posición inicial
    w_0 = 0  # velocidad angular inicial
    g = 9.8
    l = 0.6125  # este l garantiza que g/l = w0 = 16
    t = 5
    dt = 0.1


    # definimos las EDO de primer orden para usar Euler
    def f1(w):
        return w


    def f2(theta):
        return -g / l * theta


    # y el sistema de ecuaciones para usar odeint
    def eq(s, t):
        f1 = s[1]
        f2 = -g / l * s[0]
        return [f1, f2]
        # omega


    sol_pendulo = PenduloEuler(theta0, w_0, t, dt, f1, f2, eq)
    print('La solución general a la ecuación del péndulo simple es: {}'.format(sol_pendulo.sol_sympy()))

    print('El vector de desplazamiento angulas es {}'.format(sol_pendulo.euler()[0]))

    # nos interesa ver las gráficas de posición y velocidad angular
    print(sol_pendulo.graficar_theta())
    print(sol_pendulo.graficar_w())
