

from TiroParabolico import *
import matplotlib.pyplot as plt

if __name__=="__main__":

    #*******Modelacion de movimiento parbaolico simple***********
    print("***************IMPORTANTE****************************************")
    print("el sistema de referencia es positivo hacia arriba y a la derecha")
    print("*****************************************************************")

    velinit=221.7 #magnitud de la velocidad inicial
    alpha=90 #angulo de inclinacion
    g=-9.8 #aceleracion gravitacional
    h0=10 #altura inicial
    x0=15 #posicion horizontal inicial
    a=0#-3 #aceleracion del viento (+ a la derecha)

    tiro=tiroParabolico(velinit,alpha,g,h0,x0,a) #se instancia el objeto
    # Graficamos 
    tiro.figParabolico()
    

 #**********Aplicacion de caracterizacon de rifles**************
 #    
#se pone a disposicion del usuario un simulador de tiro para francotirador
#en este se asume que no hay rozamiento con el aire y que la bala es simetrica


#Escoja un modelo de rifle de la siguiente lista: AK47, AR15, M16, A91
#en h0 escriba la altura de disparo
#en x0 escriba su referente horizontal
#en viento esriba la aceleracion generada por el viento con signo (+) para viento de cola y (-) para viento de frente


    modelo="AR15"
    h0=1.5
    viento=0
    
    mirifle=rifle(modelo,h0,viento)
    mirifle.caracteristicas() #se imprime por consola algunas caracteristicas de la bala
    mirifle.figRifle() #se grafica la trayectoria de la bala 

   

   

