import numpy as np
import sympy as sym
import matplotlib.pyplot as plt

class EDO():
    def __init__(self,a,b,n,y0,f,limit=False):
        #Constructor
        self.a=a #rango de a<=x<=b
        self.b=b #rango de a<=x<=b
        self.n=n #Numero de puntos en el que se quiere partir el x,debe ser un entero
        self.y0=y0 #condición inicial en y
        self.f=f #Ecuación diferencial
        self.x= np.zeros(self.n+1)  #Arreglo de ceros para  x
        self.y= np.zeros(self.n+1)  #Arreglo de ceros para y
        self.limit=limit #punto en el que se quiere evaluar la función
    
    def h(self): #Pasos
        return (self.b-self.a)/self.n
    
    def xl(self): #arreglo de x
        if self.limit:
            self.b=self.limit
            self.x=np.arange(self.a,self.limit+self.h(),self.h())
        else:
            self.x=np.arange(self.a,self.b+self.h(),self.h())
    

    def euler(self): #Metodo de Euler
        self.xl()
        self.y[0]=self.y0 #Se agrega la condicion inicial al array de y

        for i in range(0,self.n):
            self.y[i+1]=self.y[i] + self.h() *self.f(self.x[i],self.y[i])
        return self.x,self.y
    
    def metodo_analitico(self): #Metodo analitico pararesolver EDO

        x=sym.Symbol("x")  #x variable simbolica
        y=sym.Function("y") #y función simbolica
        C1=sym.Symbol("C1") #Costante de integración de manera simbolica

        fs=self.f(x,y(x))

        fe=sym.Eq(y(x).diff(x),fs)
        con_init = {y(self.a):self.y0}     #Condiciones iniciales
        sol_ed=sym.dsolve(y(x).diff(x)-fs) #solucionar a ecuación diferencial 
        C_ed=sym.Eq(sol_ed.lhs.subs(y(x),self.y0).subs(con_init),sol_ed.rhs.subs(x,self.a)) #Reemplazar las condciones iniciales
        cons=sym.solve(C_ed)  #Determinar el valor de la costante de integración
        sol=sol_ed.subs({C1: cons[0]})   #Reemplazar la constante de integración en la solución de la ecuación diferencial
        lf=sym.lambdify(x,sol.rhs, "numpy")
        self.xl()
        ls=lf(self.x)


        return self.x, ls

    def figEDO(self): #Grafica comparación de los metodos de solución de EDO
        plt.figure(figsize=(10,8))
        plt.plot(self.euler()[0],self.euler()[1],label='Euler')
        plt.plot(self.metodo_analitico()[0],self.metodo_analitico()[1],label='Analitico')
        plt.title('Comparación de los diferentes metodos de solución de EDO')
        plt.xlabel('x')
        plt.ylabel('y')
        plt.legend()
        plt.savefig("Comparación Euler y analitico")





    

        

        

