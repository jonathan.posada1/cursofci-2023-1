import numpy as np
import sympy as sym
import matplotlib.pyplot as plt

class pendulosimple():
    def __init__(self,a,b,wn,theta0,w0,f1,f2):
        #Constructor
        self.a=a #rango de a<=t<=b
        self.b=b #rango de a<=t<=b
        self.theta0=theta0 #condición inicial en theta [rad]
        self.w0=w0 #condición inicial en la velocidad angular  [rad/s]
        self.wn=wn #Frecuencia natural del pendulo simple [rad/s]
        self.f1=f1 #Ecuación diferencial 1 de primer orden 1
        self.f2=f2 #Ecuación diferencial 2 de primer orden 1
        self.theta= [] # lista de theta
        self.w= []   # lista de w
        self.t= []   # lista de tiempo
    
    
    def tiempo(self): #intervalo de tiempo con paso de 0.1
        self.t=np.arange(self.a,self.b+0.1,0.1)
        return self.t
    

    def euler(self): #Metodo de Euler para resolver la ecuación del pendulo
        self.tiempo() #llamar la funcion tiempo
        self.theta=[self.theta0] #Se agrega la condicion inicial de theta
        self.w=[self.w0] #Se agrega la condicion inicial de w

        for i in range(0,len(self.t)-1): #Implementación de Euler en el sistema de dos ecuaciones de primer orden
            self.theta.append(self.theta[i] + 0.1 *self.f1(self.w[i]))
            self.w.append(self.w[i] + 0.1 *self.f2(self.theta[i+1]))

        return self.theta,self.w,self.t

    def metodo_analitico(self): #Metodo analitico para resolver el pendulo simple(solución general mas condicones iniciales)

        w=sym.Symbol("w")  #w variable simbolica 
        t=sym.Symbol("t")  #t variable simbolica 
        theta=sym.Function("theta") #theta función simbolica

        C1=sym.Symbol("C1") #Costante de integración 1 de manera simbolica
        C2=sym.Symbol("C2") #Costante de integración 2 de manera simbolica

        fs=-w**2 *theta(t)

        fe=sym.Eq(theta(t).diff(t,t),fs)
        sol_ed=sym.dsolve(theta(t).diff(t,t)-fs).rhs #solucionar a ecuación diferencial

        C_ed1=sym.Eq(sol_ed.subs(t,0),self.theta0)
        C_ed2=sym.Eq(sol_ed.diff(t).subs(t,0),0)

        cons=sym.solve([C_ed1,C_ed2],(C1,C2))
        sol=sol_ed.subs(cons)

        lf=sym.lambdify([t,w],sol, "numpy")
        self.tiempo()
        ls=lf(self.t,self.wn) 
        
        return self.t,ls
    
    def desplazamientoAngular(self): #Grafica des deplazamiento angular en el tiempo (Euler,analitica)
        plt.plot(self.euler()[2],self.euler()[0], label='Euler')
        plt.plot(self.metodo_analitico()[0],self.metodo_analitico()[1],label='Analitico')
        plt.title('Desplazamiento angular en el tiempo del pendulo simple')
        plt.xlabel('t (s)')
        plt.ylabel(r'$\theta(t)$ rad')
        plt.legend()
        plt.savefig("Desplazamiento angular en el tiempo del pendulo simple")


