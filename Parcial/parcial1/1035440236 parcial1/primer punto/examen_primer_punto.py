# primer punto del examen.
# realizacion del primer punto del parcial.
import math # importamos la liberia.
import numpy as np # importamos la liberia
import matplotlib.pyplot as plt # importamos la grafica

class tiroParabolico():   # clase de tiro parabolico

    def __init__(self, velinit, alpha, g, h0,x0,):
        print('Inicializando clase tiroParabolico')

        #Parámetros.

        self.velinit = velinit  # velocidad inicial
        self.radinalpha = math.radians(alpha) # angulo en radianes
        self.g = g # gravedad
        self.h0 = h0 # altura incial
        self.x0= x0 # x inicial


    def velX(self): # fedinicion de VELOCIDADENX
        vel_x = self.velinit*round(math.cos(self.radinalpha),3)
        return vel_x

    def velY(self): # VELOCIDADENY
        vel_y = self.velinit*round(math.sin(self.radinalpha),3)
        return vel_y

    def tMaxVuelo(self): #TIEMPODEVUELO
        try:
            #tmax = -2*self.velY()/self.g
            tmax = (-self.velY() - np.sqrt(self.velY()**2 - 2*self.g*self.h0))/self.g
            return tmax # retornamos el tiempo maximo.
        except: 
            return 'Error en cálculo de tmax, revisar parámetros.'
    def arrTime(self):  # arreglo del tiempo.
        aar_time=np.arange(0,self.tMaxVuelo(),0.001)   
        return aar_time
    def posX(self): #ALCANCEMAX
        posx=[self.x0+i*self.velX() for i in self.arrTime()]
        return posx
    def posY(self):#ALTURAMAX
        posy=[self.h0+i*self.velY()+ (1/2)*self.g*i**2 for i in self.arrTime()]
        return posy
    def figMp(self):  # creamos la figura correspondiente.
        plt.figure(figsize=(10,8))
        plt.plot(self.posX(), self.posY())
        plt.savefig("figura1 punto1.png")
class parabolico2(tiroParabolico):   # hacemos la herencia
    def __init__(self, velinit, alpha, g, h0, x0,aceleracion):
        super().__init__(velinit, alpha, g, h0, x0) # definimos los parametros
        self.aceleracion=aceleracion
    def posY1(self): # definimos las nuevas funciones en y
        posy=[self.h0+i*self.velY()+ (1/2)*self.g*i**2 for i in self.arrTime()]
        return posy
    def posX1(self): # definimos las nuevas funciones en x
        posx=[self.x0+i*self.velX()+ (1/2)*self.aceleracion*i**2 for i in self.arrTime()]
        return posx
    def grafica(self):    # definimos la grafica a ejecutar.
        plt.figure(figsize=(10,8))
        plt.plot(self.posX1(), self.posY1(),color="purple",lw=3)
        plt.xlabel("posicion x")
        plt.ylabel("posicion y")
        plt.grid()
        plt.savefig("figura2 punto2.png")
        # Graficamos.


       