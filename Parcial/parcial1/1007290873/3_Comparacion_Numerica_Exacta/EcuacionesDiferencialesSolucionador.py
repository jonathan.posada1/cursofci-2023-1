import numpy as np
import sympy as sym
import matplotlib.pyplot as plt


class ecuacionesDiferencialesSolucionador():
    
    """
    Clase que resolver una EDO aplicando el método numéricco de Euler, también permite resolverla de manera analítica 
    mediante la librería sympy, y comparar la solución de ambos métodos graficamente
    """

    # Constructor

    def __init__(self, x0, y0, x_inicial, x_final, n, funcion, x_evaluacion = False):

        """
        Parametros con los que se instancia la clase

        (x0, y0): condición inicial para el problema
        x_inicial: punto inicial de x del intervalo donde se resolverá la EDO
        x_final: punto final de x del intervalo donde se resolverá la EDO
        n: número de puntos en este intervalo de solución
        funcion: función lambda que define la ecuación diferencial [ dy/dx = f(x,y) ]
        x_evaluacion: Punto particular en donde se quiere evaluar la solución, si no se ingresa un valor por default es False
        """

        # Definición de atributos de instancia

        self.x0 = x0
        self.y0 = y0
        self.x_inicial = x_inicial
        self.x_final = x_final
        self.n = n
        self.funcion = funcion
        self.x_evaluacion = x_evaluacion


    # Definición de métodos

    def h(self):
        return (self.x_final - self.x_inicial) / self.n

    def DarArregloX(self):

        # Este método nos retorna el arreglo de x donde se resuleve la EDO, depende de si se quiere
        # ver la solución en un punto que no es necesariamente x_final, sino x_evaluation

        if self.x_evaluacion:
            self.x_final = self.x_evaluacion
            return (self.x_inicial, self.x_final + self.h(), self.h())  
    
        else:
            return np.arange(self.x_inicial, self.x_final + self.h(), self.h()) 


    def ResolverEuler(self):

        # Método que implementa la solución con el métodod de Euler

        # Función lambda para actualizar el arreglo de los valores de y
        y_i_plus_1 = lambda x_i, y_i: y_i + self.h() * self.funcion(x_i, y_i)

        x_array = self.DarArregloX()

        # El primer elemento del arreglo de y es la condición inicial y_0
        y_array = np.array([self.y0])

        for i in range(len(x_array) - 1):
 
            y_array = np.append(y_array, y_i_plus_1(x_array[i], y_array[i]))

        if self.x_evaluacion: 
            return y_array, y_array[-1]

        else:
            return [y_array]


    def SolucionAnalitica(self):

        # Variable y función simbólica
        xs = sym.Symbol("x")
        ys = sym.Function("y")
        
        # La función que define la ecuación diferencial en forma simbólica
        fs = self.funcion(xs, ys(xs))
        
        # Se resuelve la ecuación diferencial
        y_solucion_general = sym.dsolve(ys(xs).diff(xs) - fs)

        # Se reemplazan las condiciones iniciales en la solución
        y_sub_condiciones_inciales = y_solucion_general.subs({ys(xs): self.y0, xs: self.x0})

        # La constante de integración como variable simbólica
        C1 = sym.Symbol("C1")

        # Se obtiene el valor de la constante de acuerdo con las CI 
        C1_value = sym.solve(y_sub_condiciones_inciales, C1) 
        
        # Se reemplaza el valor de C1 en la solución
        y_solucion = y_solucion_general.subs(C1, C1_value[0]) 
        
        # Se pasa de solución simbólica a función lambda
        y_num_solucion = sym.lambdify(xs, y_solucion.rhs, "numpy")

        x_array = self.DarArregloX()
        y_array = y_num_solucion(x_array)

        if self.x_evaluacion: 
            return y_array, y_array[-1]

        else:
            return [y_array]

      
    def GraficaComparacion(self):
            
        # Arreglos para y dependiendo del método    
        y_Euler = self.ResolverEuler()[0] 
        y_solucion_analitica = self.SolucionAnalitica()[0]
        
        x = self.DarArregloX()

        # Gráfica para compara el resultado usando ambos métodos
        fig, ax=plt.subplots(figsize=(18,15))
        ax.plot(x, y_Euler, color='red', label = 'Solución Euler')
        ax.plot(x, y_solucion_analitica, color = 'blue', label = 'Solución Analítica')
        ax.set_title("Gráfica de la solución de la ED en el intervalo dado", fontsize=20)
        ax.set_xlabel("$x$",fontsize=20)
        ax.set_ylabel("$y$",fontsize=20)
        ax.legend(fontsize = 18)
        ax.grid()
        fig.savefig("comparacion_EDO_solucion.png")

        





        

