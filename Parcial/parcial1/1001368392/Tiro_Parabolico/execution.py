from MovParabolico import *
from MovParabolicoMortero import *
if __name__ == "__main__":
    
    # Datos para el movimiento parabolico y graficas
    x0 = -1
    y0 = 0
    v0 = 5
    alpha = 90
    g = -9.81
    acx = -4
    mov1 =  movparabolico(x0, y0, v0, alpha)
    # Prueba de los metodos
    print(f"Velocidad inicial en X: {mov1.v0x()}")
    print(f"Velocidad inicial en Y: {mov1.v0y()}")
    print(f"Alcance maximo: {mov1.xmax(g)}")
    print(f"Alcance maximo acelerado en X: {mov1.xmaxac(acx, g)}")
    print(f"Altura maxima: {mov1.ymax(g)}")
    print(f"Tiempo de vuelo: {mov1.flightime(g)}")
    mov1.graph(g)
    mov1.graphxac(acx, g)

    # Datos para el movimiento parabolico del mortero y graficas
    x0 = -1
    y0 = 5
    v0 = 5
    alpha = -45
    g = -9.81
    acx = -2
    mpmc = 0.3
    mov2 = movparabolicoMortero(x0, y0, v0, alpha, acx,  g, mpmc)
    # Prueba de los metodos, ahora devuelven arreglos
    print(f"Velocidad inicial en X para mortero: {mov2.v0x()}")
    print(f"Velocidad inicial en Y para mortero: {mov2.v0y()}")
    print(f"Alcance maximo para mortero: {mov2.xmax()}")
    print(f"Alcance maximo acelerado en X para mortero: {mov2.xmaxac()}")
    print(f"Altura maxima para mortero: {mov2.ymax()}")
    print(f"Tiempo de vuelo para mortero: {mov2.flightime()}")
    mov2.graph()
    mov2.graphxac()

    
