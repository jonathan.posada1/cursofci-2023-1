
# Importo librerías necesarias.
import numpy as np
import matplotlib.pyplot as plt

class TiroParabolico(): # Definición de clase.

    # Método constructor.
    def __init__(self, velinit, alpha, g, h0, x0, acevent):
        print('Inicializando clase TiroParabolico.')

        # Parámetros/Atributos de clase.

        self.velinit = velinit # Velocidad inicial.
        self.radinalpha = np.radians(alpha) # Ángulo de lanzamiento.
        self.g = g # Aceleración en Y, gravedad.
        self.h0 = h0 # Altura inicial.
        self.x0 = x0 # Posición X inicial.
        self.acvent = acevent # Aceleración en X.

    # MÃ©todos de clase.
    def VelocidadEnX(self): # Velocidad en X.
        vel_x = self.velinit*np.round(np.cos(self.radinalpha),3)
        return np.round(vel_x,4)

    def VelocidadEnY(self): # Velocidad en Y.
        vel_y = self.velinit*np.round(np.sin(self.radinalpha),3)
        return np.round(vel_y,4)

    def TiempoDeVuelo(self): # Tiempo de vuelo.
        try:
            tmax = (-self.VelocidadEnY() - np.sqrt(self.VelocidadEnY()**2 - 2*self.g*self.h0))/self.g
            return np.round(tmax,4)
        except: 
            return print('Error en calculo de tmax, revisar parametros.')

    def arrTime(self): # Arreglo de tiempo.
        t = self.TiempoDeVuelo()
        aar_time = np.arange(0, t, 0.001)
        return aar_time
    
    def posX(self): # Posición en X, movimiento uniformemente acelerado.
        posx = [self.x0 + i*self.VelocidadEnX() + (1/2)*self.acvent*i**2 for i in self.arrTime()]
        return posx

    def posY(self): # Posición en X, movimiento uniformemente acelerado.
        posy = [self.h0 + i*self.VelocidadEnY() + (1/2)*self.g*i**2 for i in self.arrTime()]
        return posy
    
    def AlcanceMax(self): # Alcance máximo.
        posx = np.array(self.posX())
        xmax = posx[-1]
        return np.round(xmax,4)
        
    def AlturaMax(self): # Altura máxima.
        posy = np.array(self.posY())
        hmax = posy.max()
        return np.round(hmax,4)

    def figMpA(self): # Gráfica de tiro parabólico.
        plt.figure(figsize=(10,8))
        plt.plot(self.posX(), self.posY())
        plt.title('Tiro Parabolico')
        plt.xlabel("Distancia X [m]")
        plt.ylabel("Distancia Y [m]")
        plt.grid()
        plt.savefig('TiroParabolico.png')

# Parte correspondiente al numeral 2.3. Herencia.

class TiroParabV2(TiroParabolico): 

    
    def __init__(self, velinit, alpha, g, h0, x0, acevent, d): # Constructor.
        print('Inicializando clase TiroParabV2, movimiento parabolico simple.')
    
        super().__init__(velinit, alpha, g, h0, x0, acevent) # Llamando atributos previos.
        # Atributos nuevos.
        self.d = d # Distancia que se quiera alcanzar.
        
    def posX(self): # Polimorfismo de clase anterior.
        posx = [self.x0 + i*self.VelocidadEnX() for i in self.arrTime()]
        return posx # Posición movimiento rectilíneo.

    def angNecesarioParaD(self): # Ángulo que se requiere para alcanzar d.
        try:
            ang = np.degrees((1/2)*np.arcsin(((-self.g*self.d)/(self.velinit)**2)))
            return print("El angulo necesario para alcanzar una distancia de {} m es de {} grados.".format(self.d, np.round(ang,4)))
        except: 
            return print('Bajo los parametros brindados, es imposible alcanzar la distancia d. Cambiar parametros de velocidad inicial.')
        
    def longTotalTrayec(self): # Longitud total de la trayectoria.
        l = ((self.velinit)**2/self.g)*(np.sin(self.radinalpha) + ((np.cos(self.radinalpha))**2)*np.arctan(np.sin(self.radinalpha)))
        return print("La longitud total de la trayectoria parabolica es de {} m.".format(abs(np.round(l,4))))