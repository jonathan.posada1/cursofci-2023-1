import matplotlib.pyplot as plt
from Movimientos2D import *

# Función de comparación entre los dos movimientos
def CompararMovimiento(x_0, y_0, v_0, angle, g=-9.8,a_x = 0):
    ''' Grafica los dos movimientos en el mismo plot'''

    # Inicializando ambos movimientos
    MovParabolico = MovimientoParabolico(x_0, y_0, v_0, angle, g=-9.8)
    MovProyectil = MovimientoProyectil(x_0, y_0, v_0, angle, g=-9.8,a_x = -1)
    
    plt.figure()
    plt.grid()
    
    # Graficando ambos, vease Plot_Movimiento.png
    MovParabolico.Graf()
    MovProyectil.Graf(color='grey',msg='Proyectil' )
    